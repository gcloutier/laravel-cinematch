-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  mer. 20 déc. 2017 à 23:17
-- Version du serveur :  10.1.28-MariaDB
-- Version de PHP :  7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tp_prog2_guillaume`
--
CREATE DATABASE IF NOT EXISTS `tp_prog2_guillaume` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `tp_prog2_guillaume`;

-- --------------------------------------------------------

--
-- Structure de la table `classements`
--

DROP TABLE IF EXISTS `classements`;
CREATE TABLE `classements` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `classements`
--

INSERT INTO `classements` (`id`, `created_at`, `updated_at`, `nom`) VALUES
(1, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Action'),
(2, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Humour'),
(3, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Aventure'),
(4, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Horreur'),
(5, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Sci-Fi'),
(6, '2017-12-01 05:00:00', '2017-12-02 05:00:00', 'Documentaire'),
(7, '2017-12-02 05:00:00', '2017-12-03 05:00:00', 'Famille');

-- --------------------------------------------------------

--
-- Structure de la table `critiques`
--

DROP TABLE IF EXISTS `critiques`;
CREATE TABLE `critiques` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `film_id` int(10) UNSIGNED NOT NULL,
  `vote` decimal(2,1) NOT NULL,
  `commentaires` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `derniereModif` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `critiques`
--

INSERT INTO `critiques` (`id`, `created_at`, `updated_at`, `utilisateur_id`, `film_id`, `vote`, `commentaires`, `derniereModif`) VALUES
(1, '2017-12-14 01:44:15', '2017-12-20 20:12:30', 4, 11, '4.5', 'holly moolllyyyyyasdasdasdasdsa', '2017-12-20 15:12:30'),
(3, '2017-12-14 10:38:56', '2017-12-14 10:38:56', 5, 11, '5.0', 'yababababadouuuuuuuu', '2017-12-14 05:38:56'),
(10, '2017-12-18 22:19:26', '2017-12-18 22:19:26', 3, 11, '3.5', 'asdsadsadsadsd', '2017-12-18 17:19:26'),
(12, '2017-12-19 01:01:10', '2017-12-19 01:01:10', 4, 12, '3.5', 'Ma critique de starswars', '2017-12-18 20:01:09'),
(13, '2017-12-20 20:12:48', '2017-12-20 20:12:48', 4, 11, '5.0', 'une autre critique', '2017-12-20 15:12:48'),
(14, '2017-12-20 20:12:56', '2017-12-20 20:12:56', 4, 11, '1.0', 'une autre', '2017-12-20 15:12:56'),
(15, '2017-12-20 20:13:04', '2017-12-20 20:13:04', 4, 11, '4.5', 'asdasdasdasdasdsad', '2017-12-20 15:13:04');

-- --------------------------------------------------------

--
-- Structure de la table `films`
--

DROP TABLE IF EXISTS `films`;
CREATE TABLE `films` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `titre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `anneepar` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `synopsis` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duree` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acteurs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `classement_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `cover_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actifBoolean` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `films`
--

INSERT INTO `films` (`id`, `created_at`, `updated_at`, `titre`, `anneepar`, `synopsis`, `duree`, `acteurs`, `classement_id`, `user_id`, `cover_image`, `actifBoolean`) VALUES
(11, '2017-12-07 07:42:44', '2017-12-19 01:43:46', 'Logan', '2017', 'ok !!!', '90', 'sadasd', 2, 3, 'logan_1513172416_1513173576.jpg', 1),
(12, '2017-12-19 01:00:19', '2017-12-19 01:00:19', 'Star wars 8', '2017', 'asdasdasdasdasdasdsad', '111', 'Luke, Shae et les autres', 1, 3, 'starwars_1513627219.jpeg', 1);

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2010_11_19_213602_create_roles_table', 1),
(2, '2012_11_17_185132_create_classements_table', 1),
(3, '2013_11_10_024730_create_films_table', 1),
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2015_11_19_205809_create_critiques_table', 1),
(6, '2017_10_12_100000_create_password_resets_table', 1),
(7, '2017_11_15_023830_ajouter_util_id_a_films', 1),
(8, '2017_12_06_234508_add_cover_image_to_films', 2),
(9, '2017_12_11_165341_add_actifBoolean_to_films', 3),
(10, '2017_12_20_144948_updateCritiqueCommantaire', 4);

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `roles`
--

INSERT INTO `roles` (`id`, `created_at`, `updated_at`, `nom`) VALUES
(1, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Membre'),
(2, '2017-12-04 23:09:38', '2017-12-04 23:09:38', 'Administrateur');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'momou', 'mo@mo.ca', '$2y$10$FP0Tmynd/dgVjomjbaQGW.6h9.pginVY88ZVYgpwh1oNimQJWO6Z.', 2, 'xTjCSEb4quEo5z88NV2uEFxlxtOsaFzQ8M5SjPBjk83e9CNZfpqqlchIJrfK', '2017-12-07 02:31:15', '2017-12-07 02:55:27'),
(4, 'Guillaume', 'gui@gui.com', '$2y$10$SGnRyGyLttN6c4o2xJubzeEgzSDl5k66Z1Ci3dzboS0IWDzFJGSEm', 1, 'GqUX0lNvON535SpbZZDBPaegIJIGM3ZTHMn71Ap9VuItT2CYHQaRa6wk6ZoQ', '2017-12-14 01:33:41', '2017-12-14 01:33:41'),
(5, 'marylou', 'mary@mary.com', '$2y$10$/iFsEZVisgpIcJUGBiax6Odowar919GuWrBrjNF3t.SA1BBtUiylm', 1, NULL, '2017-12-14 10:38:11', '2017-12-14 10:38:11');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `classements`
--
ALTER TABLE `classements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `critiques`
--
ALTER TABLE `critiques`
  ADD PRIMARY KEY (`id`),
  ADD KEY `critiques_utilisateur_id_foreign` (`utilisateur_id`),
  ADD KEY `critiques_film_id_foreign` (`film_id`);

--
-- Index pour la table `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`),
  ADD KEY `films_classement_id_foreign` (`classement_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`name`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `classements`
--
ALTER TABLE `classements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `critiques`
--
ALTER TABLE `critiques`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT pour la table `films`
--
ALTER TABLE `films`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `critiques`
--
ALTER TABLE `critiques`
  ADD CONSTRAINT `critiques_film_id_foreign` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`),
  ADD CONSTRAINT `critiques_utilisateur_id_foreign` FOREIGN KEY (`utilisateur_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `films`
--
ALTER TABLE `films`
  ADD CONSTRAINT `films_classement_id_foreign` FOREIGN KEY (`classement_id`) REFERENCES `classements` (`id`);

--
-- Contraintes pour la table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
