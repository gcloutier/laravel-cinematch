<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Critique;

class Film extends Model
{
    // nom de la table
    protected $table = 'films';

    // clé primaire que l'on peut spécifier si on veut...
    public $primaryKey = 'id';

    // timestamp
    public $timestamps = true;

    public function user(){
      return $this->belongsTo('App\User');
    }

    public function classement(){
      // je veux pouvoir aller chercher le classement
      return $this->belongsTo('App\Classement','classement_id');
    }

    public function critiquesFilm(){
      // je veux pouvoir aller chercher le classement
      return $this->hasmany('App\Critique');
    }



    public function critiques(){
      // si je veux chercher les critiques associer au films j'utilise hasmany + class name
      // du coté de critique pas besoin de belongsTo dans ce cas ci...
      return $this->hasMany('App\Critique');
    }





    // films = nom de la table

}
