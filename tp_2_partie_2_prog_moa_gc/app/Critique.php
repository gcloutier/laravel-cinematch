<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Critique extends Model
{


    public function User(){
      // 'utilisateur_id' c'est le id du User, si c'est pas là en paramètres !?? Ça ne marche pas ...!
      return $this->belongsTo('App\User','utilisateur_id');
    }

    public function Film(){
      // 'utilisateur_id' c'est le id du User, si c'est pas là en paramètres !?? Ça ne marche pas ...!
      return $this->belongsTo('App\Film','film_id');

    }
}
