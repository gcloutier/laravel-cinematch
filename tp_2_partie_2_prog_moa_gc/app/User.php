<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function films(){
        return $this->hasMany('App\Film');
    }

    public function isAdmin(){

      // return $this->role_id;
      if (Auth::user()->role_id == 2) {
        return true;
      }
      else {
        return false;
      }
      // si User = role->id  = est égale a 2
      // return true
      // else
      // return false
    }
}
