<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classement extends Model
{
    // protected $table = 'classements';

    public function films(){
        return $this->hasMany('App\Film');
    }
}
