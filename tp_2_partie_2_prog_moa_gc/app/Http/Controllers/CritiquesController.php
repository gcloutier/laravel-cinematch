<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Film;
use App\Critique;

class CritiquesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
         // validation:
         $this->validate($request,[
             'commentaires' => 'nullable|max:255',
             'vote' => 'required'
         ]);

         // pour enregistrer dans la DB:
         // $film = Film::find($id);

         $critique = new Critique;
         $critique->film_id=$request->input('noFilm');
         $critique->commentaires =$request->input('commentaires');
         $critique->vote =$request->input('vote');
         $critique->derniereModif=Carbon::now();
         // prendrait note ici... !
         $critique->utilisateur_id = auth()->user()->id;

         $critique->save();
       return back()->with('success', 'Critique publiée');
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
         $critique_id = Critique::find($id);
         $select = $critique_id->vote;

         return view('films.editCritique')->with('critique',$critique_id)->with('select', $select);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
       $this->validate($request,[
           'commentaires' => 'nullable|max:255',
           'vote' => 'required'
       ]);

       $critique = Critique::find($id);
       $critiquemsg = $critique->film_id;

       $critique->commentaires =$request->input('commentaires');
       $critique->vote =$request->input('vote');
       $critique->derniereModif=Carbon::now();

       $critique->save();

       return redirect('/films/'.$critiquemsg)->with('success', 'Critique modifiée');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $critique = Critique::find($id);
        $critiquemsg = $critique->film_id;
        $critique->delete();
        return redirect('/films/'.$critiquemsg)->with('success', 'Critique supprimée');
    }
}
