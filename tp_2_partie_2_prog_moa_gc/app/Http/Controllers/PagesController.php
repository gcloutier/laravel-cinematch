<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        // return 'Ce que je veux que ça retourne';

        // return une vue: (dans le dossier ressources>view>pages>...)
        $titre = "test variable titre !";
        // compact permet l'utilisation de ma variable dans ma vue mais besoin de {{$titre}} à l'endroit ou je veux utiliser ma var
        // return view('pages.index',compact('titre'));

        // avec ->with plutot que compact()
        return view('pages.index')->with('titre',$titre);
        
    }
    public function about(){
        // ici trouver une autre page que about...
        return view('pages.about');
    }
    public function services(){
        $data = array(
            'titre' => 'Services fafdasdsad',
            // pour faire un array...
            'services' => ['service 1','service 2','Service 3 etc...']
        );
        

        // data est seulement un array qui est utiliser dans mon controller, dans ma vue j'utilise {{$titre}} ou {{$services}} (dans une boucle)
        return view('pages.services')->with($data);
    }
}
