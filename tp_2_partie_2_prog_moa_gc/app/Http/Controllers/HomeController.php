<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Critique;
use App\Film;
// use App\Classement;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        // User c'est le model
        $user = User::find($user_id);

        $critiques = Critique::all();

        if ($user->role_id === 2) {
          return view('home')->with('films', $user->films)->with('user', $user)->with('critiques', $critiques);
        }
        return view('home')->with('user', $user)->with('critiques', $critiques);
    }

}
