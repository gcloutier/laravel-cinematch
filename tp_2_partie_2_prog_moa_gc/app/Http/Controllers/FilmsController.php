<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// import de la librairy storage (pour effacer les images)
use Illuminate\Support\Facades\Storage;
// import du model "Film" namespace 'App' a toute les fois que je fais référence au model Film
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Film;
use App\Classement;
use App\Critique;
use Config;

class FilmsController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        Carbon::setLocale(Config::get('app.locale'));
        Carbon::setToStringFormat('d/m/Y à H:i:s');
        // ajout d'exeption pour voir les films et la page individuel avec un array 'except'
        // $this->middleware('auth', ['except' => ['index', 'search','searchajax','show']]);
        $this->middleware('admin',['only'=>'create','store','destroy','show','edit','update','searchajax','search']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // eloquant pour aller chercher les donnees dans la bd;
        // va chercher tout dans la table film :
        // $films = Film::all();
        // c'est ici que je fais ma requête (la bonne !!! )
        $films = Film::orderby('anneepar','desc')->get();
        // pour limiter le nombre de film
        // $films = Film::orderby('anneepar','desc')->take(1)->get();

        // Pour créer une pagination
        // $films = Film::orderby('anneepar','desc')->paginate(1); (1)c'est la limite ...

        // autre requête possible ex.:
        // return $films = Film::where('titre','Thor : Ragnarok')->get();
        // ou
        // $films = \DB::select('SELECT * FROM films');  sans le backslash marche pas...

        // on passe la variable à ma vue films/index.blade.php
        return view('films.index')->with('films',$films);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('films.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if(auth()->user()->role_id === 1){
          // redirection de vue avec redirect --- voir avec le message d'erreur 'error', 'message'
          return redirect('/films')->with('error', 'Page non autorisée');
      }
        // validation:
        $this->validate($request,[
            'titre' => 'required',
            'anneepar' => 'required|numeric',
            'synopsis' => 'required',
            'duree' => 'required|numeric',
            'acteurs' => 'required',
            'type' => 'required',
            // faut que soit un image, non obligatoir,
            'cover_image' => 'image|nullable|max:1999'
        ]);

        // Prise en charge de l'upload :
        if($request->hasFile('cover_image')){
          // comment avoir un nom de fichier avec l'extension
          $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
          // juste avoir nom fichier (function php)
          $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
          // juste avoir extension (Laravel)
          $extension = $request->file('cover_image')->getClientOriginalExtension();
          // filename to Store
          $fileNameToStore = $fileName.'_'.time().'.'.$extension;
          // upload l'imagesy
          $path = $request->file('cover_image')->storeAs('/public/cover_images', $fileNameToStore);
        }else {
          $fileNameToStore = 'noimage.jpg';
        }

        // pour enregistrer dans la DB:

        $film = new Film;
        $film->titre =$request->input('titre');
        $film->anneepar =$request->input('anneepar');
        $film->synopsis =$request->input('synopsis');
        $film->duree =$request->input('duree');
        $film->acteurs =$request->input('acteurs');
        $film->user_id = auth()->user()->id;
        $film->classement_id =$request->input('type');
        $film->cover_image = $fileNameToStore;
        $film->actifBoolean = 1;

        $film->save();
        return redirect('films/')->with('success', 'Film enregistré');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        Carbon::setLocale('fr');


        $avgVote = Critique::avg('vote');

        $film_id = Film::find($id);

        return view('films.detail')->with('film',$film_id)->with('vote',$avgVote);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // permet d'identifier que c'est le bon user qui change le film - (Variable du id du film)
        $film_id = Film::find($id);

        if(auth()->user()->id !== $film_id->user_id){
            // redirection de vue avec redirect --- voir avec le message d'erreur 'error', 'message'
            return redirect('/films')->with('error', 'Page non autorisée');
        }

        return view('films.edit')->with('film',$film_id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // copier depuis la fonction store

        // validation:
        $this->validate($request,[
            'titre' => 'required',
            'anneepar' => 'required|numeric',
            'synopsis' => 'required',
            'duree' => 'required|numeric',
            'acteurs' => 'required',
            'type' => 'required',
        ]);

        // Prise en charge de l'upload :
        if($request->hasFile('cover_image')){
          // comment avoir un nom de fichier avec l'extension
          $fileNameWithExt = $request->file('cover_image')->getClientOriginalName();
          // juste avoir nom fichier (function php)
          $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
          // juste avoir extension (Laravel)
          $extension = $request->file('cover_image')->getClientOriginalExtension();
          // filename to Store
          $fileNameToStore = $fileName.'_'.time().'.'.$extension;
          // upload l'image
          $path = $request->file('cover_image')->storeAs('/public/cover_images', $fileNameToStore);
        }

        // pour enregistrer dans la DB:

        $film = Film::find($id);
        $film->titre =$request->input('titre');
        $film->anneepar =$request->input('anneepar');
        $film->synopsis =$request->input('synopsis');
        $film->duree =$request->input('duree');
        $film->acteurs =$request->input('acteurs');
        $film->classement_id =$request->input('type');
        if($request->hasFile('cover_image')){
          $film->cover_image = $fileNameToStore;
        };


        $film->save();

        return redirect('/films')->with('success', 'Film modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // permet d'identifier que c'est le bon user qui change le film - (Variable du id du film)
        $film = Film::find($id);

        // permet d'identifier que c'est le bon user qui change le film
        if (Auth::user()->role_id == 1) {
            // redirection de vue avec redirect --- voir avec le message d'erreur 'error', 'message'
            return redirect('/films')->with('error', 'Page non autorisée');
        }

        $film->actifBoolean = 0;
        $film->save();

        return redirect('/films')->with('success', 'Film supprimé');
    }


    public function searchajax(Request $request)
    {
      $search = $request->input('search');

      $films = Film::where('titre', 'LIKE', "%{$search}%")
        ->orWhere('synopsis', 'LIKE', "%{$search}%")
        // ->orderby('anneepar','desc') deja dans la function index...
        ->get();

      return response()->json($films);
    }



    public function search()
    {
        return view('films.search');
    }
}
