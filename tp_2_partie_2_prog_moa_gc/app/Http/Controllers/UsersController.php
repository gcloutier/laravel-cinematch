<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateAccount;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// import du model "Film" namespace 'App';

// a toute les fois que je fais référence au model User
use App\User;

class UsersController extends Controller
{
  public function __construct()
  {
      // middleware qui empêche un guest d'accéder à toute page pour éditer un user (exepter searchuserajax qui permet de savoir quel user il y a dans la db)
      $this->middleware('auth', ['except' => ['searchuserajax']]);
  }

  public function show($id)
  {
      //
  }

  public function edit($id)
  {
    $users = User::find($id);
        return view('users.edit',compact('users','id'));
  }

  public function update(UpdateAccount $request, $id)
    {
      // var_dump($request);
      $usersForm = $request->all();

        $users = User::find($id);

    		$users->update($usersForm);


        // return view('auth/login')->with('users',$users);
        Auth::logout();
        return redirect('login/')->with('success', 'Veuillez vous reconnecter');
    }





  public function searchuserajax()
  {
    $rechercheInput = $_POST['rechercheInput'];

    // $films = \App\Film::all();
    // $films = DB::table('films') ->where('titre', 'like', 'b%') ->get();

    $users= User::where('name','like',$rechercheInput.'%')->get();

    return response()->json($users);

  }


}
