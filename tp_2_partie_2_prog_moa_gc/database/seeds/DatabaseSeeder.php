<?php

use Carbon\Carbon;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('roles')->insert([
          ['nom' => 'Membre','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
          ['nom' => 'Administrateur','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
      ]);

      DB::table('classements')->insert([
          ['nom' => 'G','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
          ['nom' => '13+','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
          ['nom' => '16+','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],
          ['nom' => '18+','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')],

      ]);
        DB::table('films')->insert([
            ['titre' => 'Thor : Ragnarok', 'anneepar' => '2017', 'synopsis' => "Après avoir constaté la mort de leur père, le dieu Odin, Thor et son frère Loki sont transportés sur une mystérieuse planète gouvernée par un hurluberlu qui organise des combats de gladiateurs. Thor doit alors combattre son ami Hulk, considéré comme le guerrier le plus puissant et acclamé par les habitants de l'endroit. Pendant ce temps, sur Asgard, la déesse de la mort engendre un règne de terreur. Thor devra trouver un moyen de s'échapper de la planète qui a fait de lui un esclave afin d'aller défendre son royaume et son peuple, en perdition.",
            'duree' => '191', 'acteurs' => "Chris Hemsworth, Tom Hiddleston, Cate Blanchett, Tessa Thompson, Jeff Goldblum, Mark Ruffalo.",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'user_id' => '1', 'classement_id' => '2', 'cover_image' => 'thor.jpg', 'actifBoolean' => '1' ],
            ['titre' => 'Deadpool', 'anneepar' => '2016', 'synopsis' => "Wade Wilson, ancien membre des forces spéciales, découvre qu'il est atteint d'un cancer en phase terminale. Devant sa fin imminente, il accepte de suivre un traitement mis au point par le machiavélique Ajax. Grâce aux procédés génétiques, le jeune homme devient un mutant doté de la capacité de guérir ses blessures instantanément, mais il est complètement défiguré. Pour interrompre les expériences qui vont le transformer en esclave, il s'évade du laboratoire secret. Wade Wilson cache la laideur de son visage avec un masque et endosse un costume rouge. Sous le surnom Deadpool, il se lance à la poursuite du malfaisant Ajax et de sa bande.",
            'duree' => '108', 'acteurs' => "Ryan Reynolds, Morena Baccarin, Gina Carano, T.J. Miller, Ed Skrein",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'user_id' => '1', 'classement_id' => '2','cover_image' => 'deadpool.jpg', 'actifBoolean' => '1'  ],
            ['titre' => 'Seul sur Mars', 'anneepar' => '2015', 'synopsis' => "Lors d’une expédition sur Mars, l’astronaute Mark Watney (Matt Damon) est laissé pour mort par ses coéquipiers, une tempête les ayant obligés à décoller en urgence. Mais Mark a survécu et il est désormais seul, sans moyen de repartir, sur une planète hostile. Il va devoir faire appel à son intelligence et son ingéniosité pour tenter de survivre et trouver un moyen de contacter la Terre. A 225 millions de kilomètres, la NASA et des scientifiques du monde entier travaillent sans relâche pour le sauver, pendant que ses coéquipiers tentent d’organiser une mission pour le récupérer au péril de leurs vies.",
            'duree' => '141', 'acteurs' => "Matt Damon, Jessica Chastain, Kristen Wiig, Jeff Daniels, Michael Peña, Sean Bean, Kate Mara, Sebastian Stan, Mackenzie Davis, Chiwetel Ejiofor",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'user_id' => '1', 'classement_id' => '2', 'cover_image' => 'Seul-sur-Mars.jpg', 'actifBoolean' => '1'  ]
        ]);

        DB::table('critiques')->insert([
            ['vote' => '4.5', 'commentaires' => 'Ce film est super.', 'derniereModif' => "2017-12-20 18:36:49",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'utilisateur_id' => '2', 'film_id' => '1'],
            ['vote' => '4.0', 'commentaires' => "Super film d'action. Je recommande.", 'derniereModif' => "2017-12-20 18:37:42",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'utilisateur_id' => '1', 'film_id' => '1'],
            ['vote' => '3.0', 'commentaires' => "Moi j'ai trouvé ça moyen", 'derniereModif' => "2017-12-20 18:37:42",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'utilisateur_id' => '2', 'film_id' => '1'],
            ['vote' => '2.0', 'commentaires' => "bof ! pas super", 'derniereModif' => "2017-12-20 18:37:42",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'utilisateur_id' => '1', 'film_id' => '1'],
            ['vote' => '5.0', 'commentaires' => "Ah ouais super bon !!!", 'derniereModif' => "2017-12-20 18:37:42",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'), 'utilisateur_id' => '2', 'film_id' => '1']
          ]);

    }
}
