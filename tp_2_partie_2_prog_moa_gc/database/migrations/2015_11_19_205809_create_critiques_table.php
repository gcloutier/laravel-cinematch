<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCritiquesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('critiques', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('utilisateur_id')->unsigned();
            $table->integer('film_id')->unsigned();
            $table->decimal('vote',2,1);
            $table->string('commentaires');
            $table->dateTime('derniereModif');
        });

        Schema::table('critiques', function ($table) {
             $table->foreign('utilisateur_id')->references('id')->on('users');
          });
        Schema::table('critiques', function ($table) {
            $table->foreign('film_id')->references('id')->on('films');
          });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('critiques');
    }
}
