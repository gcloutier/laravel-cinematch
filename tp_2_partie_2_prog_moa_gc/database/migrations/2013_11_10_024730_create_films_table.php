<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('films', function (Blueprint $table) {
            // clé primaire
            $table->increments('id');
            $table->timestamps();
            $table->string('titre', 50);
            $table->string('anneepar', 4);
            $table->string('synopsis',1000);
            $table->string('duree',3);
            $table->string('acteurs',255);
            $table->integer('classement_id')->unsigned();
        });
        
        Schema::table('films', function ($table) {
             $table->foreign('classement_id')->references('id')->on('classements');
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('films');
    }
}
