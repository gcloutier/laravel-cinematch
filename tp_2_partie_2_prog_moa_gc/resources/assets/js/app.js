
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.addEventListener("DOMContentLoaded",init, false);
// function init()
// {
// 	$('#name').keyup(function(){
// 	if ($("#name").val().length >2) {
// 		alert('sadasdasdsad')
// 	}
// 	});
// }
//
//

$( document ).ready(function() {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $("#search").keyup(function(){

      var valeurInput = $(this).val();
      var longueurInput = valeurInput.length;
      if (longueurInput >= 3) {
          // pour lancer ajax
          $('#search-form').submit();
      } else {
        // on vide la liste
        $('#search-result').text('');
      }

    });




    // 1. soumettre le formulaire
    $('#search-form').on('submit', function (event) {
      event.preventDefault();
      // .text c'est plus rapide

      $.ajax({
        type: "POST",
        url: "/searchajax",
        data: {
          search: $('#search').val()
        },
        dataType: "json",
        success: function(response) {
          // console.log(response);
          $('#search-result').text('');
          var html = [];

          for (var i = 0; i < response.length; i++) {
            console.log(response[i]);
            var title = response[i].titre;
            var year = response[i].anneepar;
            var id = response[i].id;
            var image = response[i].cover_image;

            // s'affiche plus vite dans le DOM avec html.push
              html.push('<div class="row contour mb-2">');

                  html.push('<div class="thumbnail col-md-2">');
                    html.push('<a href="/films/' + id + '"><img src="/storage/cover_images/' + image+'" alt="' + title + '"></a>');
                  html.push('</div>');

                  html.push('<div class="col-md-10">');
                      html.push('<h3>' + title + '</h3>');
                      html.push('<h3>Année de parution :' + year + '</h3>');
                  html.push('</div>');

              html.push('</div>');

          };
          console.log(html);
          // ne pas oublier ('') pour enlever les virgules
          $('#search-result').append(html.join(''));
        }
      });
    });
  });




  $("#name").keyup(function(){

    var valeurInput = $("#name").val();
    var longueurInput = valeurInput.length;
    if (longueurInput >= 3) {
        // pour lancer ajax
        $('#search-form').submit();
        } else {
        // on vide la liste
        $('#search-result').text('');
    }
  });


  $("#name").keyup(function(){
    var valeurInput = $("#name").val();
    var longueurInput = valeurInput.length;
    if (longueurInput >= 3) {
      $.ajax({
        type:"POST",
        url:"/searchuserajax",
        data:{rechercheInput:valeurInput},
        dataType:"json",
        success:function(jsonResultat)
        {
          for (var i = 0; i < jsonResultat.length; i++) {
            // entré des résultat json dans la liste (uniquement le titre)
            if(jsonResultat[i].name){
              $('.result-user').removeClass('hidden');
              $("#search-result-user").html('Utilisateur existant');
            }
		      }
        }
      })
    }
    else{
      $('.result-user').addClass('hidden');
      $("#search-result-user").text('');
    }
  });


  //
  // https://stackoverflow.com/questions/1053902/how-to-convert-a-title-to-a-url-slug-in-jquery
  function convertToSlug(Text) {
      return Text
          .toLowerCase()
          // enlève tous ce qui est pas lettre et espace
          .replace(/[^\w ]+/g,'')
          // change tous les espaces ou plus par " - "
          .replace(/ +/g,'-');
  }
