<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    {{--  <link rel="stylesheet" href="{{asset('css/custom.css')}} ">  --}}
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,800,900" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

        {{--  nom de mon appli dans .env  --}}
        <title>{{config('app.name','CinéMatch')}}</title>

    <!-- Styles -->

</head>
<body>
    <div class="background">
        <div id="app">
            @include('inc.navbar')
            <div class="container container_gris">
            {{--  <div></div>  --}}
            <div class="row">
                <div class="col-md-12 logo">
                    <img src="{{asset('img/logo.png')}}">
                </div>
            </div>
            @include('inc.messages')
            {{--  créer une section contenu dans mes vues --}}
            @yield('content')
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    {{--  <script src="{{ asset('js/monscript.js') }}"></script>  --}}
</body>
</html>
