@extends('layouts.app')

@section('content')
    <h1>Mise à jour de vos informations</h1>

    {!! Form::open(['action' => ['UsersController@update', $users->id], 'method' => 'PUT']) !!}

        {{--  Titre  --}}
        <div class="form-group">
            <h2>{{Form::label('name','Nom:')}}</h2>
            {{Form::text('name',$users->name,['class' => 'form-control', 'placeholder' => 'Nom'])}}
        </div>

        <div class="form-group">
            <h2>{{Form::label('email','Courriel:')}}</h2>
            {{Form::text('email',$users->email,['class' => 'form-control', 'placeholder' => 'Courriel'])}}
        </div>



        {{Form::submit('Modifier les informations',['class' => 'btn btn-primary'])}}

    {!! Form::close() !!}


@endsection
