{{--  fonction pour faire une extension de mon layout  --}}
@extends('layouts.app')
    

{{--  le yield dans mon layout principale   --}}
@section('contenu')
    {{--    --}}
    <h1>{{$titre}}</h1>

    
    @if(count($services) > 0)
        <ul class="list-group">
        @foreach($services as $service)
            <li class="list-group-item">{{$service}}</li>
        @endforeach
        </ul>
    @endif


    
@endsection