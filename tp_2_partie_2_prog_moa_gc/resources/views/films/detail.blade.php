@extends('layouts.app')

@section('content')
    <div class="row mt-5">
                <div class="col-sm-4">
                {{-- <img src="{{asset('img/'.str_slug($film->titre, '-'))}}.jpg"> --}}
                <img class="full_img" src="/storage/cover_images/{{$film->cover_image}}" alt="{{$film->cover_image}}">
                </div>

                <div class="col-sm-6">
                    <p><a href="/films" class="btn btn-primary" role="button">Retour</a></p>
                    <h3>{{$film->titre}}</h3>
                    <p class="bold">Synopsis:</p>
                    <p>{{$film->synopsis}}</p>

                    <p><span class="bold">Note:</span>
                      @if ($film->critiques->count() >= 5)
                      {{-- moyenne des votes --}}
                        @for ($i=0.5; $i < 5; $i++)
                          @if ($vote > $i)
                          <span class="fa fa-star checked"></span>
                        @elseif($vote >= $i)
                          <span class="fa fa-star-half-o"></span>
                          @else
                          <span class="fa fa-star"></span>
                          @endif
                        @endfor
                      {{--  nombre de votes : --}}
                      / sur {{$film->critiques->count()}} votes
                      @else
                        @for ($i=0; $i < 5; $i++)
                          <span class="fa fa-star"></span>
                        @endfor
                      @endif
                    </span></p>

                    <p><span class="bold">Genre: </span> {{$film->classement->nom}} </p>

                    <p><span class="bold">Année de parution: </span> {{$film->anneepar}}</p>
                    <p><span class="bold">Durée: </span> {{$film->duree}} minutes</p>
                    <p><span class="bold">Acteurs: </span> {{$film->acteurs}}</p>


                    <hr>
                    {{--  si n'est pas un invité (guest) ne peut pas voir ça --}}
                    @if(!Auth::guest())
                        {{--  si le film n'est pas au user (id)  = ne peut pas modifier ou deleté   --}}
                        @if(Auth::user()->id == $film->user_id)
                            <a href="/films/{{$film->id}}/edit" class="btn btn-default">Modifier</a>


                            {{--  pour permettre d'effacer un film  --}}
                            {!!Form::open(['action' =>['FilmsController@destroy', $film->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                {{Form::hidden('_method','DELETE')}}
                                {{Form::submit('Effacer', ['class' => 'btn btn-danger'])}}
                            {!!Form::close()!!}
                        @endif
                    @endif
                </div>

    </div>
    <br>
    <div class="row">
      <div class="col-xs-12">
        <ul class="list-group">
          @foreach ($film->critiques->sortByDesc('created_at') as $critique)
            <li class="list-group-item clearfix">
                {{-- User c'est pas dans mysql, c'est la function dans mon model...   --}}
                <span class="bold_comment">{{$critique->User->name}}</span> :
                {{$critique->commentaires}}

                <span class="date">{{$critique->created_at->format('j F Y')}}</span>


                {{--  Pour afficher la note (en étoiles non-éditable )--}}
                @php $select = $critique->vote @endphp
                @for ($i=0.5; $i < 5; $i++)
                  @if ($select > $i)
                  <span class="fa fa-star checked"></span>
                @elseif($select >= $i)
                  <span class="fa fa-star-half-o"></span>
                  @else
                  <span class="fa fa-star"></span>
                  @endif
                @endfor


              @if(!Auth::guest())
                  {{--  si le film n'est pas au user (id)  = ne peut pas modifier ou deleté   --}}
                  @if(Auth::user()->id == $critique->utilisateur_id)

                      {!! Form::open(['method' => 'DELETE', 'url' => ['films/'.$critique->id.'/deletecritique'],'class' => 'pull-right']) !!}
                        {{-- {{Form::hidden('_method','DELETE')}} --}}
                        {!! Form::submit('Effacer', ['class' => 'btn btn-danger']) !!}
                      {!! Form::close() !!}

                      <a href="/films/{{$critique->id}}/editCritique" class="btn btn-default pull-right mr-2">Modifier</a>
                  @endif
              @endif


            </li>
          @endforeach
        </ul>
      </div>
    </div>

      {{--  Ajouter une critique --}}
      @if(!Auth::guest())
        <div class="row">
          <div class="col-xs-12">
            <h3>Faire une critique</h3>
            {{--  envoie à la fonction store de CritiquesController  --}}

            <form class="" action="/films/{{ $film->id }}/critiques" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="noFilm" value="{{ $film->id }}">
              <div class="form-group">
                <textarea name="commentaires" class="form-control"></textarea>
              </div>
              {{-- <div class="form-group">
                <input type="number" name="vote" min="0" max="5" required></input>
              </div> --}}

                <div class="demo">
                  <div class="ratingControl">
                      <input type="radio" id="rating-5" name="vote" value="5">
                      <label class="ratingControl-stars ratingControl-stars--5" for="rating-5">5</label>
                      <input type="radio" id="rating-45" name="vote" value="4.5">
                      <label class="ratingControl-stars ratingControl-stars--45 ratingControl-stars--half" for="rating-45">45</label>
                      <input type="radio" id="rating-4" name="vote" value="4">
                      <label class="ratingControl-stars ratingControl-stars--4" for="rating-4">4</label>
                      <input type="radio" id="rating-35" name="vote" value="3.5">
                      <label class="ratingControl-stars ratingControl-stars--35 ratingControl-stars--half" for="rating-35">35</label>
                      <input type="radio" id="rating-3" name="vote" value="3">
                      <label class="ratingControl-stars ratingControl-stars--3" for="rating-3">3</label>
                      <input type="radio" id="rating-25" name="vote" value="2.5">
                      <label class="ratingControl-stars ratingControl-stars--25 ratingControl-stars--half" for="rating-25">25</label>
                      <input type="radio" id="rating-2" name="vote" value="2">
                      <label class="ratingControl-stars ratingControl-stars--2" for="rating-2">2</label>
                      <input type="radio" id="rating-15" name="vote" value="1.5">
                      <label class="ratingControl-stars ratingControl-stars--15 ratingControl-stars--half" for="rating-15">15</label>
                      <input type="radio" id="rating-1" name="vote" value="1">
                      <label class="ratingControl-stars ratingControl-stars--1" for="rating-1">1</label>
                      <input type="radio" id="rating-05" name="vote" value="0.5">
                      <label class="ratingControl-stars ratingControl-stars--05 ratingControl-stars--half" for="rating-05">05</label>
                  </div>
                </div>

                {{-- <textarea name="vote" class="form-control"></textarea> --}}


              <div class="form-group commentaire">
                <button type="submit" class="btn btn-primary">Envoyer le commentaire</button>
              </div>

            </form>
          </div>
        </div>
      @endif

@endsection
