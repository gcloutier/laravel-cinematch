@extends('layouts.app')

@section('content')
    <h1>Rechercher</h1>
        {{--  {!! Form::open(['url' => '/searchajax', 'id' => 'search-form']) !!}  --}}
        {!! Form::open(['id' => 'search-form']) !!}
            {!! Form::label('search','Nom du film') !!}
            {!! Form::text('search') !!}
            {{--  {!! Form::submit('Rechercher') !!}  --}}
        {!! Form::close() !!}

    <div class="row">
        <div class="col-md-12">
        <div class="pd-2">
            <div id="search-result">
                <!--   ce qui sera retourné par json -->
            </div>
        </div>
        </div>
    </div>
@endsection
