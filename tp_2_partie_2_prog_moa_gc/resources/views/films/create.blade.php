@extends('layouts.app')

@section('content')
    <h1>Créer un nouveau film</h1>
    {{--  envoie à la fonction store de FIlmsController  --}}
    {!! Form::open(['action' => 'FilmsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

        {{--  Titre  --}}
        <div class="form-group">
            <h2>{{Form::label('titre','Titre du film:')}}</h2>
            {{Form::text('titre','',['class' => 'form-control', 'placeholder' => 'Titre'])}}
        </div>

         {{--  Année  --}}
         <div class="form-group">
            {{Form::label('anneepar','Année de parution:')}}
            {{Form::text('anneepar','',['class' => 'form-control', 'placeholder' => 'Année', 'maxlength' => '4'])}}
        </div>

        {{--  Synopsis  --}}
        <div class="form-group">
            {{Form::label('synopsis','Synopsis:')}}
            {{Form::textarea('synopsis','',['class' => 'form-control', 'placeholder' => 'Synopsis'])}}
        </div>

        {{--  duree  --}}
        <div class="form-group">
            {{Form::label('duree','Durée (en minute):')}}
            {{Form::text('duree','',['class' => 'form-control', 'placeholder' => 'Durée', 'maxlength' => '3'])}}
        </div>

        <div class="form-group">
            {{Form::label('Acteurs','Acteurs:')}}
            {{Form::textarea('acteurs','',['class' => 'form-control', 'placeholder' => 'Acteurs'])}}
        </div>

        <div class="form-group">
            {{Form::label('Type','Type de film:')}}
            {{Form::select('type',[
              '0' => 'Selectionnez un type de film',
              '1' => 'G',
              '2' => '13+',
              '3' => '16+',
              '4' => '18+',
            ])}}
        </div>
        <div class="form-group">
          {{Form::file('cover_image')}}
        </div>

        {{Form::submit('Envoyer',['class' => 'btn btn-primary'])}}

    {!! Form::close() !!}



@endsection
