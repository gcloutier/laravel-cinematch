@extends('layouts.app')

@section('content')
    <h1>films</h1>

    <div class="row">
    @if(count($films) > 0)
        @foreach($films as $film)
          @if ($film->actifBoolean == 1)
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                <div class="crop-container">
                    {{-- <img src="img/{{str_slug($film->titre, '-')}}.jpg" alt="{{$film->titre}}"> --}}
                    <img class="full_img" src="/storage/cover_images/{{$film->cover_image}}" alt="{{$film->cover_image}}">
                </div>
                <div class="caption">
                    <h3>{{$film->titre}}</h3>

                    <p>{{ str_limit($film->synopsis, 100) }}<a href='/films/{{$film->id}}'>(lire la suite)</a></p>
                    {{-- le lien '<a>' va chercher en post le $id (dans le Filmscontroller function 'show' )  )   --}}
                </div>
                </div>
            </div>
            @endif
        @endforeach
        {{--  pour pagination si besoin !!!!  --}}
        {{--  {{$films->links()}}  --}}
    @else
        <p class='error'>Il n'y a aucun film enregistré</p>
    @endif
    </div>
@endsection
