@extends('layouts.app')

@section('content')
    <h1>Modifier le film</h1>
    {{--  envoie à la fonction update de FIlmsController  --}}
    {{--  $film->id pour savoir quel film modifier --}}
    {!! Form::open(['action' => ['FilmsController@update', $film->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

        {{--  Titre  --}}
        <div class="form-group">
            <h2>{{Form::label('titre','Titre du film:')}}</h2>
            {{Form::text('titre',$film->titre,['class' => 'form-control', 'placeholder' => 'Titre'])}}
        </div>

         {{--  Année  --}}
         <div class="form-group">
            {{Form::label('anneepar','Année de parution:')}}
            {{Form::text('anneepar',$film->anneepar,['class' => 'form-control', 'placeholder' => 'Année', 'maxlength' => '4'])}}
        </div>

        {{--  Synopsis  --}}
        <div class="form-group">
            {{Form::label('synopsis','Synopsis:')}}
            {{Form::textarea('synopsis',$film->synopsis,['class' => 'form-control h10', 'placeholder' => 'Synopsis'])}}
        </div>

        {{--  duree  --}}
        <div class="form-group">
            {{Form::label('duree','Durée (en minute):')}}
            {{Form::text('duree',$film->duree,['class' => 'form-control', 'placeholder' => 'Durée', 'maxlength' => '3'])}}
        </div>

        <div class="form-group">
            {{Form::label('Acteurs','Acteurs:')}}
            {{Form::textarea('acteurs',$film->acteurs,['class' => 'form-control h10', 'placeholder' => 'Acteurs'])}}
        </div>

        <div class="form-group">
            {{Form::label('Type','Type de film:')}}
            {{Form::select('type',[
              $film->classement_id => $film->classement->nom,
              '1' => 'G',
              '2' => '13+',
              '3' => '16+',
              '4' => '18+',
            ])}}
        </div>

        <div class="row">
          <div class="col-sm-3">
            <div class="icon">
              <p>Image actuelle: </p>
              <img class="img-thumbnail icon" src="/storage/cover_images/{{$film->cover_image}}" alt="{{$film->cover_image}}">
            </div>
          </div>
          <div class="col-sm-9">
            {{Form::file('cover_image')}}
            <div class="pull-right">
              {{Form::hidden('_method','PUT')}}
              {{Form::submit('Modifier le film',['class' => 'btn btn-primary'])}}
            </div>
          </div>
        </div>

        {{--  pour permettre la modification  --}}


    {!! Form::close() !!}

@endsection
