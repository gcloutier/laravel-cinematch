@extends('layouts.app')

@section('content')

    <h1>Modifier le commentaire</h1>
    {{--  envoie à la fonction update de FIlmsController  --}}
    {{--  $film->id pour savoir quel film modifier --}}

    <form class="" action="{!! action('CritiquesController@update', ['id' => $critique->id]) !!}" method="POST">

    {{ csrf_field() }}
    {{-- <input type="hidden" name="noFilm" value="{{ $film->id }}"> --}}
      <div class="form-group">
        <textarea name="commentaires" class="form-control">{{$critique->commentaires}}</textarea>
      </div>
      {{-- <div class="form-group">
        <input type="number" name="vote" min="0" max="5" required></input>
      </div> --}}

      {{-- Pour afficher le nombre d'étoile de la critique --}}
      @for ($i = 1; $i < 11; $i++)
        @if ($select == $i/2)
          @php ($checked = $i)@endphp
        @endif
      @endfor

      {{-- @if ($checked === 2) patate @endif --}}
        <div class="demo">
          <div class="ratingControl">
              <input @if ($checked === 10) checked="checked" @endif type="radio" id="rating-5" name="vote" value="5">
              <label class="ratingControl-stars ratingControl-stars--5" for="rating-5">5</label>
              <input @if ($checked === 9) checked="checked" @endif type="radio" id="rating-45" name="vote" value="4.5">
              <label class="ratingControl-stars ratingControl-stars--45 ratingControl-stars--half" for="rating-45">45</label>
              <input @if ($checked === 8) checked="checked" @endif type="radio" id="rating-4" name="vote" value="4">
              <label class="ratingControl-stars ratingControl-stars--4" for="rating-4">4</label>
              <input @if ($checked === 7) checked="checked" @endif type="radio" id="rating-35" name="vote" value="3.5">
              <label class="ratingControl-stars ratingControl-stars--35 ratingControl-stars--half" for="rating-35">35</label>
              <input @if ($checked === 6) checked="checked" @endif type="radio" id="rating-3" name="vote" value="3">
              <label class="ratingControl-stars ratingControl-stars--3" for="rating-3">3</label>
              <input @if ($checked === 5) checked="checked" @endif type="radio" id="rating-25" name="vote" value="2.5">
              <label class="ratingControl-stars ratingControl-stars--25 ratingControl-stars--half" for="rating-25">25</label>
              <input @if ($checked === 4) checked="checked" @endif type="radio" id="rating-2" name="vote" value="2">
              <label class="ratingControl-stars ratingControl-stars--2" for="rating-2">2</label>
              <input @if ($checked === 3) checked="checked" @endif type="radio" id="rating-15" name="vote" value="1.5">
              <label class="ratingControl-stars ratingControl-stars--15 ratingControl-stars--half" for="rating-15">15</label>
              <input @if ($checked === 2) checked="checked" @endif type="radio" id="rating-1" name="vote" value="1">
              <label class="ratingControl-stars ratingControl-stars--1" for="rating-1">1</label>
              <input @if ($checked === 1) checked="checked" @endif type="radio" id="rating-05" name="vote" value="0.5">
              <label class="ratingControl-stars ratingControl-stars--05 ratingControl-stars--half" for="rating-05">05</label>
          </div>
        </div>

        {{-- <textarea name="vote" class="form-control"></textarea> --}}


      <div class="form-group">
        <button type="submit" class="btn btn-primary">Envoyer le commentaire</button>
      </div>

    </form>



    {{-- {!! Form::open(['action' => ['CritiquesController@update', $critique->id], 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!} --}}

        {{--  Titre  --}}
        {{-- <div class="form-group">
            <h2>{{Form::label('titre','Titre du film:')}}</h2>
            {{Form::text('titre',$critique->commentaires,['class' => 'form-control', 'placeholder' => 'Titre'])}}
        </div> --}}

        {{--  comment je rentre derniereModif ???--}}



        {{--  pour permettre la modification  --}}
        {{-- {{Form::hidden('_method','PUT')}}


        {{Form::submit('Modifier le commentaire',['class' => 'btn btn-primary'])}}

    {!! Form::close() !!} --}}

@endsection
