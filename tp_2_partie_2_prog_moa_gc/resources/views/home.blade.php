@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


        @if(isset($_GET['msg']))
            <div class="alert alert-success"><p>Connexion réussi</p></div>
        @endif
            <div class="panel panel-default">
                <div class="panel-heading">Bureau</div>
                <table class="table table-striped">

                @foreach ($critiques as $critique)
                  @if ($critique->utilisateur_id == $user->id)
                  <tr>
                    <td><span class="bold_comment">Commentaire pour le film: </span>
                        {{$critique->Film->titre}}
                    </td>
                    <td>
                        {{$critique->commentaires}}
                    </td>
                    <td class="pull-right">
                      <a href="/films/{{$critique->Film->id}}" class="btn btn-default">Voir le commentaire</a>
                    </td>
                  </tr>
                @endif

                @endforeach

                </table>
                @if ($user->role_id === 2)
                <div class="panel-body">


                    <a href="/films/create" class="btn btn-primary">Entrer un film</a>
                    <h3>Vos films:</h3>
                    @if(count($films) > 0)
                    <table class="table table-striped">
                        <tr>
                            <th>Titre</th>
                            <th></th>
                            <th></th>
                        </tr>

                        @foreach($films as $film)
                          @if ($film->actifBoolean == 1)
                            <tr>
                                <td>{{$film->titre}}</td>
                                <td><a href="/films/{{$film->id}}/edit" class="btn btn-default">Modifier</a></td>
                                <td>
                                    {!!Form::open(['action' =>['FilmsController@destroy', $film->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                        {{Form::hidden('_method','DELETE')}}
                                        {{Form::submit('Effacer', ['class' => 'btn btn-danger'])}}
                                    {!!Form::close()!!}
                                </td>
                            </tr>
                          @endif
                        @endforeach
                    </table>
                    @else
                        <p>Aucun film enregistré</p>
                    @endif
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
