<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('css/app.css')}} ">
        <link rel="stylesheet" href="{{asset('css/custom.css')}} ">

        {{--  nom de mon appli dans .env  --}}
        <title>{{config('app.name','CinéMatch')}}</title>

        
    </head>
    <body>
        <div class="container">
            <h1>Page non trouvée !</h1>
            <a href='/'>Retour</a>
        </div>
    </body>
</html>

