<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// le nom du controlleur et la méthode  entre ' ' :
Route::get('/', 'FilmsController@index');
// Route::get('/about', 'PagesController@about');
// Route::get('/services', 'PagesController@services');

// Route::get('/users/edit', 'UsersController@edit');
Route::name('language')->get('language/{lang}', 'HomeController@language');

Route::resource('/users','UsersController');

Route::resource('/films', 'FilmsController');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// page de recherche
Route::get('/search', 'FilmsController@search')->name('search');

// recherche ajax
Route::post('/searchajax', 'FilmsController@searchajax');

Route::post('/searchuserajax', 'UsersController@searchuserajax');

Route::post('/films/{film}/critiques' , 'CritiquesController@store');
Route::get('/films/{critique}/editCritique' , 'CritiquesController@edit');
Route::post('films/{critique}/updatecritiques' , 'CritiquesController@update');

Route::delete('films/{critique}/deletecritique' , 'CritiquesController@destroy');
// Route::resource('/films', 'CritiquesController');

// Route::post('/searchuserajax',)
